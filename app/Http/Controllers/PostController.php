<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;



class PostController extends Controller
{
    /**

    * @method  bill_show

    * @use To show the bill

    * Returns the view

    * Created by Naveen S

    **/

    public function bill_show(){

    	return view('bill');

    }

    /**
		
	* @method bill_valid

	* @use to valid the date

	*Return the billing cycle count

	*Created by Naveen S

    **/

    private $count = 0;

    public function bill_valid(Request $request){

    	$start_date = $request->start_date;

    	//dd($start_date);

    	

    	$this->validate($request, [

    		'start_date' => 'required|date',

    		'end_date' => 'required|date|after:start_date',

    	],
    	[
    		'start_date.required' => 'Starting Date is important',

    		'end_date.required' => 'Finishing Date is important',

    		'end_date.after' => 'Date must be end after the start date'
    	]);

    	$s_date = \Carbon\Carbon::parse($request->start_date);

    	$e_date = \Carbon\Carbon::parse($request->end_date);

		$start_day = $s_date->day;

		$start_month = $s_date->month;

		$start_year = $s_date->year;	



		$end_day = $e_date->day;

		$end_month = $e_date->month;

		$end_year = $e_date->year;


		$make_day = 20; // Starting

		$end_make_day = 19;


		if($start_year == $end_year){

			$this->calc1($start_month,$end_month, $start_day, $end_day, $start_year, $end_year);

		}
		elseif ($end_year >= $start_year+1) {

			if($end_year!=$start_year+1){

				$this->count = ($end_year-$start_year-1)*12;

				$this->count = $this->count + 12 - $start_month;

				$this->count = $this->count + $end_month;

			}			
			else{

				$this->count = $this->count + 12 - $start_month;

				$this->count = $this->count + $end_month-1;	

			}
			//dd($start_month);

			$this->calc($start_month, $end_month, $start_day, $end_day, $start_year, $end_year);

		}

		$response = ['from_date' => $request->start_date, 'end_date' => $request->end_date, 'cycles' => $this->count];

		return response()->json($response, 200);

    }




    /**

    * @method calc

    * @params start_month, $end_month, $start_day, $end_day

    * Use to calculate month count

    **/

    public function calc($start_month, $end_month, $start_day, $end_day, $start_year, $end_year){
    	
		if($end_day>=20){

			$this->count = $this->count + 1;

		}

		if($start_day<20){

			$this->count = $this->count +1 ;

		}			

		return $this->count;

    }



    public function calc1($start_month, $end_month, $start_day, $end_day, $start_year, $end_year){
    	
    	
    	if($end_month>=$start_month+1){

			$this->count = $this->count + $end_month - $start_month;

			if($end_day>=20){

				$this->count = $this->count + 1;

			}

			if($start_day<20){

				$this->count = $this->count +1 ;

			}

		}
		else{

				if($start_day<20){

					$this->count = $this->count +1 ;
					
				}
				if($end_day>19){

					$this->count = $this->count +1 ;

				}

		}

		return $this->count;

    }

    
}

//dd($this->count);