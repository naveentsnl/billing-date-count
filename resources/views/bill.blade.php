<!DOCTYPE html>
<html>
<head>
	<title>Billing Date</title>
</head>
<body>

	<h1>Billing Count</h1>

	<h4> Billing cycles start every 20th of the month. and ends every 19th of the month</h4>

	<form action="/bill_check" method="POST">

		{{ csrf_field() }}
		
		<input type="date" name="start_date"> <br><br>

		<input type="date" name="end_date"> <br><br>

		<input type="submit" name="submit">

	</form>

	@foreach ($errors->all() as $error)

		<li>{{ $error }}</li>

	@endforeach

</body>
</html>